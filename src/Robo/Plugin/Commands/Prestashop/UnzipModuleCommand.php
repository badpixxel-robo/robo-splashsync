<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Robo\Splash\Robo\Plugin\Commands\Prestashop;

use Robo\Collection\CollectionBuilder;
use Robo\Symfony\ConsoleIO;
use Robo\Task\Archive\Extract;
use Robo\Task\Filesystem\FilesystemStack;
use Robo\Tasks;
use Symfony\Component\Console\Command\Command;

/**
 * Unzip Module & Install on Prestashop.
 */
class UnzipModuleCommand extends Tasks
{
    /**
     * @command prestashop:unzip
     *
     * @description Deploy Prestashop Module for Production
     */
    public function unzip(ConsoleIO $console, string $source, string $prjDir = null): int
    {
        $installDir = ($prjDir ?? '/var/www/html')."/modules";
        //====================================================================//
        // Single File Request
        if (is_file($source)) {
            return $this->unzipFile($console, $source, $installDir);
        }
        if (is_dir($source)) {
            $scan = array_diff(scandir($source) ?: array(), array('..', '.')) ;
            foreach ($scan as $file) {
                if ("zip" == pathinfo($source."/".$file, PATHINFO_EXTENSION)) {
                    $this->unzipFile($console, $source."/".$file, $installDir);
                }
            }

            return 0;
        }
        $console->error("Module Install Failed");

        return 1;
    }

    /**
     * Unzip a Single File
     *
     * @description Deploy Prestashop Module for Production
     */
    protected function unzipFile(ConsoleIO $console, string $source, string $installDir): int
    {
        $console->section("Unzip module & Install on Prestashop");
        //====================================================================//
        // Extract Module Code
        $moduleCode = (pathinfo($source, PATHINFO_FILENAME));
        //====================================================================//
        // User Infos
        $console->definitionList(
            array("Source" => $source),
            array("Prestashop Dir" => $installDir),
            array("Module Code" => $moduleCode)
        );
        //====================================================================//
        // Create Tasks Collection
        /** @var CollectionBuilder $collection */
        $collection = $this->collectionBuilder($console);
        //====================================================================//
        // Clean Target Directory
        $collection->addTask(
            $this->taskDeleteDir($installDir."/".$moduleCode)
        );
        //====================================================================//
        // Extract Files to Directory
        /** @var Extract $extract */
        $extract = $this->taskExtract($source);
        $collection->addTask(
            $extract->to($installDir."/".$moduleCode)
        );
        //====================================================================//
        // Change Owner
        /** @var FilesystemStack $chown */
        $chown = $this->taskFilesystemStack();
        $collection->addTask(
            $chown->chown($installDir."/".$moduleCode, "www-data", true)
        );
        /** @var FilesystemStack $group */
        $group = $this->taskFilesystemStack();
        $collection->addTask(
            $group->chgrp($installDir."/".$moduleCode, "www-data", true)
        );
        //====================================================================//
        // Execute Tasks
        if (!$collection->run()->wasSuccessful()) {
            $console->error(sprintf("Module %s Install Failed", $moduleCode));

            return 1;
        }

        return 0;
    }
}
