<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Robo\Splash\Robo\Plugin\Commands\Prestashop;

use Robo\Symfony\ConsoleIO;
use Robo\Task\Base\Exec;
use Robo\Task\Composer\CreateProject;
use Robo\Tasks;
use Symfony\Component\Console\Command\Command;

/**
 * Deploy Splash Sync Module for Prestashop.
 */
class DeployModuleCommand extends Tasks
{
    /**
     * @command prestashop:deploy
     *
     * @description Deploy Prestashop Module for Production
     */
    public function deploy(ConsoleIO $console, string $version = null, bool $dev = false, string $prjDir = null): int
    {
        $version ??= '@stable';
        $prjDir ??= '/var/www/html';
        $tmpPath = $this->_tmpDir();
        $installDir = $prjDir."/modules/splashsync";
        //====================================================================//
        // Init
        $console->title("Deploy Splash module for Prestashop");
        $console->definitionList(
            array("Module Version" => $version),
            array("Include Dev" => ($dev ? "<info>YES</info>" : "<comment>No</comment>")),
            array("Prestashop Dir" => $prjDir),
            array("Temporary Dir" => $tmpPath),
            array("Module Dir" => $installDir)
        );
        //====================================================================//
        // Create Composer Project
        /** @var CreateProject $createProjectTask */
        $createProjectTask = $this->taskComposerCreateProject();
        $createProjectTask
            ->source('splash/prestashop')
            ->version($version)
            ->target($tmpPath)
            ->dev($dev)
            ->noInteraction()
            ->disablePlugins()
            ->run()
        ;
        //====================================================================//
        // Move project to Prestashop Modules Dir
        if (is_dir($installDir)) {
            $this->_cleanDir($installDir);
        }
        $this->_mirrorDir($tmpPath."/modules/splashsync", $installDir);
        //====================================================================//
        // List Installed Files
        /** @var Exec $execTask */
        $execTask = $this->taskExec('ls');
        $execTask->arg('-l')->arg($installDir)->run();
        //====================================================================//
        // Notify User
        $console->success(sprintf("Splash Module %s deployed in %s", $version, $installDir));

        return Command::SUCCESS;
    }
}
