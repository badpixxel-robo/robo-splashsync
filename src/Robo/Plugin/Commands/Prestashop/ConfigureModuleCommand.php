<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Robo\Splash\Robo\Plugin\Commands\Prestashop;

use BadPixxel\Robo\Splash\MySqlTasksTrait;
use BadPixxel\Robo\Splash\Robo\Plugin\Tasks\MySql\ExecSqlTask;
use Robo\Symfony\ConsoleIO;
use Robo\Task\Base\Exec;
use Robo\Tasks;
use Symfony\Component\Console\Command\Command;

/**
 * Configure Prestashop from Env Variables
 */
class ConfigureModuleCommand extends Tasks
{
    use MySqlTasksTrait;

    /**
     * @command prestashop:configure
     *
     * @description Configure Prestashop from Env Variables
     */
    public function run(ConsoleIO $consoleIo): int
    {
        $parameters = array(
            'SPLASH_WS_ID' => null,
            'SPLASH_WS_KEY' => null,
            'SPLASH_LANG_ID' => 'en-US',
            'SPLASH_USER_ID' => '1',
            'SPLASH_WS_HOST' => null,
        );
        //====================================================================//
        // Detect Parameters
        foreach ($parameters as $name => $dfValue) {
            $parameters[$name] = getenv($name) ?: $dfValue;
        }
        $parameters = array_filter($parameters);
        //====================================================================//
        // Require Expert Mode
        if (!empty($parameters['SPLASH_WS_HOST'])) {
            $parameters['SPLASH_WS_EXPERT'] = "1";
        }
        //====================================================================//
        // Create Tasks Collection
        $collection = $this->collectionBuilder($consoleIo);
        foreach ($parameters as $name => $value) {
            /** @var Exec $task */
            $task = $this->taskExec(sprintf(
                "php bin/console prestashop:config set %s --value=%s",
                $name,
                $value
            ));

            $collection->addTask($task->silent(true));
        }
        //====================================================================//
        // Execute Tasks
        if ($collection->run()->wasSuccessful()) {
            $consoleIo->success("Prestashop Config Done");
        } elseif (!$this->tryWithMySql($consoleIo, $parameters)) {
            $consoleIo->error("Prestashop Config Failed");
        }

        return Command::SUCCESS;
    }

    private function tryWithMySql(ConsoleIO $consoleIo, array $parameters): bool
    {
        //====================================================================//
        // Configure Database
        self::setDatabaseConfig(
            (string) getenv("DB_SERVER"),
            getenv("DB_PORT") ?: null,
            getenv("DB_USER") ?: null,
            getenv("DB_PASSWD") ?: null
        );
        //====================================================================//
        // Create Tasks Collection
        $collection = $this->collectionBuilder($consoleIo);
        foreach ($parameters as $name => $value) {
            /** @var ExecSqlTask $task */
            $task = $this->taskMySqlExecTask();
            $task
                ->setDatabaseName((string) getenv("DB_NAME"))
                ->setSql(sprintf(
                    "INSERT INTO %sconfiguration ( name ,  value ,  date_add ,  date_upd )"
                    ." VALUES ('%s','%s',NOW(), NOW());",
                    getenv("DB_PREFIX"),
                    $name,
                    $value
                ))
            ;
            $collection->addTask($task);
        }
        //====================================================================//
        // Execute Tasks
        if ($collection->run()->wasSuccessful()) {
            $consoleIo->success("Prestashop Config Done");

            return true;
        }

        return false;
    }
}
