<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Robo\Splash\Robo\Plugin\Commands\Prestashop;

use BadPixxel\Robo\Splash\MySqlTasksTrait;
use BadPixxel\Robo\Splash\PrestashopTasksTrait;
use Robo\Symfony\ConsoleIO;
use Symfony\Component\Console\Command\Command;

/**
 * Deploy Splash Sync Module for Prestashop.
 */
class InstallDockerCommand extends \Robo\Tasks
{
    use MySqlTasksTrait;
    use PrestashopTasksTrait;

    /**
     * @command prestashop:install:docker
     *
     * @description Deploy Prestashop Module for Production
     */
    public function run(ConsoleIO $consoleIo, bool $force = false): int
    {
        //====================================================================//
        // Notify User
        if (!$force && is_file("./config/settings.inc.php")) {
            $consoleIo->success("Prestashop already installed");

            return Command::SUCCESS;
        }
        $consoleIo->title("Prestashop Install");
        //====================================================================//
        // Configure Database
        self::setDatabaseConfig(
            (string) getenv("DB_SERVER"),
            getenv("DB_PORT") ?: null,
            getenv("DB_USER") ?: null,
            getenv("DB_PASSWD") ?: null
        );
        //====================================================================//
        // Create Install Tasks Collection
        $collection = $this->collectionBuilder($consoleIo);
        //====================================================================//
        // Base Tasks
        $collection
            ->progressMessage("Install Prestashop Sources")
            ->addTask($this->taskPrestashopDockerMoveDirectoriesTask())
            ->progressMessage("Detect Hostname")
            ->addTask($this->taskPrestashopDockerDetectHostnameTask())
            ->progressMessage("Connect to Database")
            ->addTask($this->taskMySqlWaitForServerTask())
            ->progressMessage("Ensure Database Exists")
            ->addTask(
                $this->taskMySqlEnsureDatabaseTask()->setDatabaseName((string) getenv("DB_NAME"))
            )
        ;
        //====================================================================//
        // Erase database if requested
        if (!empty(getenv("PS_ERASE_DB"))) {
            $collection
                ->progressMessage("Reset Database")
                ->addTask(
                    $this->taskMySqlDropDatabaseTask()->setDatabaseName((string) getenv("DB_NAME"))
                )
                ->addTask(
                    $this->taskMySqlCreateDatabaseTask()->setDatabaseName((string) getenv("DB_NAME"))
                )
            ;
        }
        //====================================================================//
        // Install Prestashop
        $collection
            ->progressMessage("Install Prestashop from CLI")
            ->addTask($this->taskPrestashopDockerInstallTask())
        ;
        //====================================================================//
        // Execute Tasks
        if (!$collection->run()->wasSuccessful()) {
            $consoleIo->error("Prestashop Install Failed");

            return Command::FAILURE;
        }
        $consoleIo->success("Prestashop Install Done");

        return Command::SUCCESS;
    }
}
