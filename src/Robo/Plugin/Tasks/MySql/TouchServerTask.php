<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Robo\Splash\Robo\Plugin\Tasks\MySql;

use Robo\Result;
use Robo\Task\Base\Exec;

/**
 * Check MySql Server is Available
 */
class TouchServerTask extends AbstractMySqlTask
{
    /**
     * Touch Database Server
     *
     * @inheritDoc
     */
    public function run(): Result
    {
        /** @var Exec $task */
        $task = $this
            ->taskExec(sprintf("%s -e 'status'", $this->getCommand()))
        ;

        return $task
            ->silent(true)
            ->run()
        ;
    }
}
