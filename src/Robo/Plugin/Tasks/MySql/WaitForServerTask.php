<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Robo\Splash\Robo\Plugin\Tasks\MySql;

use BadPixxel\Robo\Splash\MySqlTasksTrait;
use Robo\Result;

/**
 * Execute a Single Grumphp Task
 */
class WaitForServerTask extends AbstractMySqlTask
{
    use MySqlTasksTrait;

    /**
     * @inheritDoc
     */
    public function run(): Result
    {
        for ($i = 0; $i < 10; $i++) {
            //====================================================================//
            // Touch Database
            if ($this->taskMySqlTouchServerTask()->run()->wasSuccessful()) {
                return Result::success($this, "Database Connected");
            }
            sleep(5);
        }

        return Result::error($this, "Unable to connect database");
    }
}
