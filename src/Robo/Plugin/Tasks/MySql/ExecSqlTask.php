<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Robo\Splash\Robo\Plugin\Tasks\MySql;

use Robo\Result;
use Robo\Task\Base\Exec;

/**
 * Execute Database Raw Query
 */
class ExecSqlTask extends AbstractMySqlTask
{
    /**
     * Database Name
     */
    protected string $sql;

    /**
     * Configure Raw Query
     */
    public function setSql(string $sql): self
    {
        $this->sql = $sql;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function run(): Result
    {
        /** @var Exec $task */
        $task = $this->taskExec(sprintf(
            '%s -D %s -e "%s"',
            $this->getCommand(),
            $this->dbName ?? "undefined",
            $this->sql
        ));

        return $task
            ->silent(true)
            ->run()
        ;
    }
}
