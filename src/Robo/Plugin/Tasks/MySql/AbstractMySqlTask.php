<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Robo\Splash\Robo\Plugin\Tasks\MySql;

use Robo\Contract\BuilderAwareInterface;
use Robo\LoadAllTasks;
use Robo\Task\BaseTask;

abstract class AbstractMySqlTask extends BaseTask implements BuilderAwareInterface
{
    use LoadAllTasks;

    /**
     * Database Name
     */
    protected string $dbName;

    /**
     * Database Configuration
     *
     * @var array<string, null|string>
     */
    private static array $dbConfig = array();

    /**
     * Configure Database Connexion
     */
    public static function setDatabaseConfig(
        string $dbServer,
        string $dbPort = "3306",
        string $dbUser = null,
        string $dbPass = null
    ): void {
        self::$dbConfig = array_filter(array(
            "server" => $dbServer,
            "port" => $dbPort,
            "user" => $dbUser,
            "pass" => $dbPass,
        ));
    }

    /**
     * Configure Database Connexion
     *
     * @return static
     */
    public function setDatabaseName(string $dbName): self
    {
        $this->dbName = $dbName;

        return $this;
    }

    /**
     * Get MySql Server Access Console Command
     */
    protected function getCommand(string $binary = "mysql"): string
    {
        $command = sprintf("%s -h %s -P %s", $binary, self::$dbConfig["server"], self::$dbConfig["port"]);

        if (!empty(self::$dbConfig["user"])) {
            $command .= sprintf(" -u %s", self::$dbConfig["user"]);
        }
        if (!empty(self::$dbConfig["pass"])) {
            $command .= sprintf(" -p%s", self::$dbConfig["pass"]);
        }

        return $command;
    }

    /**
     * Get MySql Server Access Console Command
     */
    protected function getAdminCommand(): string
    {
        return $this->getCommand("mysqladmin");
    }
}
