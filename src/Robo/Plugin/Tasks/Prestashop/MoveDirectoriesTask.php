<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Robo\Splash\Robo\Plugin\Tasks\Prestashop;

use Robo\Contract\BuilderAwareInterface;
use Robo\LoadAllTasks;
use Robo\Result;
use Robo\Task\BaseTask;
use Robo\Task\Filesystem\FilesystemStack;

/**
 * Move Docker Files for Install
 */
class MoveDirectoriesTask extends BaseTask implements BuilderAwareInterface
{
    use LoadAllTasks;

    /**
     * @inheritDoc
     */
    public function run(): Result
    {
        /** @var FilesystemStack $filesystemTask */
        $filesystemTask = $this->taskFilesystemStack();
        //====================================================================//
        // Copy Source files
        $filesystemTask
            ->mirror('/tmp/data-ps/prestashop', '/var/www/html')
            ->copy('/tmp/defines_custom.inc.php', '/var/www/html/config/defines_custom.inc.php', true)
        ;
        //====================================================================//
        // Move Install Folder
        if (("install" !== getenv("PS_FOLDER_INSTALL")) && is_dir("/var/www/html/install")) {
            $filesystemTask->rename(
                "/var/www/html/install",
                "/var/www/html/".getenv("PS_FOLDER_INSTALL"),
                true
            );
        }
        //====================================================================//
        // Move Admin Folder
        if (("admin" !== getenv("PS_FOLDER_ADMIN")) && !is_dir("/var/www/html/".getenv("PS_FOLDER_ADMIN"))) {
            $filesystemTask->rename(
                "/var/www/html/admin",
                "/var/www/html/".getenv("PS_FOLDER_ADMIN"),
                true
            );
        }
        //====================================================================//
        // Execute Stack
        if (!$filesystemTask->run()->wasSuccessful()) {
            Result::error($this, "Unable to copy all sources files");
        }

        return Result::success($this, "Source files copied");
    }
}
