<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Robo\Splash\Robo\Plugin\Tasks\Prestashop;

use Robo\Contract\BuilderAwareInterface;
use Robo\LoadAllTasks;
use Robo\Result;
use Robo\Task\BaseTask;

/**
 * Detect Hostname
 */
class DetectHostnameTask extends BaseTask implements BuilderAwareInterface
{
    use LoadAllTasks;

    /**
     * @inheritDoc
     */
    public function run(): Result
    {
        if ("<to be defined>" !== getenv("PS_DOMAIN")) {
            return $this->_exec("export PS_DOMAIN=$(hostname -i)");
        }

        return Result::success($this);
    }
}
