<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Robo\Splash\Robo\Plugin\Tasks\Prestashop;

use Robo\Contract\BuilderAwareInterface;
use Robo\LoadAllTasks;
use Robo\Result;
use Robo\Task\Base\Exec;
use Robo\Task\BaseTask;

/**
 * Install Prestashop
 */
class InstallTask extends BaseTask implements BuilderAwareInterface
{
    use LoadAllTasks;

    /**
     * @inheritDoc
     */
    public function run(): Result
    {
        /** @var Exec $task */
        $task = $this->taskExec(sprintf(
            "runuser -g www-data -u www-data -- php /var/www/html/%s/index_cli.php",
            getenv("PS_FOLDER_INSTALL")
        ));

        $task
            ->rawArg("--domain=".getenv("PS_DOMAIN"))
            ->rawArg("--db_server=".getenv("DB_SERVER").":".getenv("DB_PORT"))
            ->rawArg("--db_name=".getenv("DB_NAME"))
            ->rawArg("--db_password=".getenv("DB_PASSWD"))
            ->rawArg("--prefix=".getenv("DB_PREFIX"))
            ->rawArg("--firstname=John")
            ->rawArg("--lastname=Doe")
            ->rawArg("--password=".getenv("ADMIN_PASSWD"))
            ->rawArg("--email=".getenv("ADMIN_MAIL"))
            ->rawArg("--language=".getenv("PS_LANGUAGE"))
            ->rawArg("--country=".getenv("PS_COUNTRY"))
            ->rawArg("--all_languages=".getenv("PS_ALL_LANGUAGES"))
            ->rawArg("--newsletter=0")
            ->rawArg("--send_email=0")
            ->rawArg("--ssl=".getenv("PS_ENABLE_SSL"))
        ;

        return $task->run();
    }
}
