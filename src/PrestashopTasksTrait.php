<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Robo\Splash;

use BadPixxel\Robo\Splash\Robo\Plugin\Tasks\Prestashop;
use Robo\Collection\CollectionBuilder;

/**
 * Trait for Prestashop Tasks in Commands
 */
trait PrestashopTasksTrait
{
    /**
     * Move Directories
     *
     * @return CollectionBuilder|Prestashop\MoveDirectoriesTask
     */
    protected function taskPrestashopDockerMoveDirectoriesTask()
    {
        return $this->task(Prestashop\MoveDirectoriesTask::class);
    }

    /**
     * Detect Docker Hostname
     *
     * @return CollectionBuilder|Prestashop\DetectHostnameTask
     */
    protected function taskPrestashopDockerDetectHostnameTask()
    {
        return $this->task(Prestashop\DetectHostnameTask::class);
    }

    /**
     * Execute Install Script
     *
     * @return CollectionBuilder|Prestashop\InstallTask
     */
    protected function taskPrestashopDockerInstallTask()
    {
        return $this->task(Prestashop\InstallTask::class);
    }
}
