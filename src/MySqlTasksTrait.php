<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Robo\Splash;

use BadPixxel\Robo\Splash\Robo\Plugin\Tasks\MySql;
use Robo\Collection\CollectionBuilder;

/**
 * Trait for Using MySql Tasks in Commands
 */
trait MySqlTasksTrait
{
    /**
     * Configure Database Connexion
     */
    public static function setDatabaseConfig(
        string $dbServer,
        ?string $dbPort = null,
        string $dbUser = null,
        string $dbPass = null
    ): void {
        $dbPort ??= "3306";

        MySql\AbstractMySqlTask::setDatabaseConfig($dbServer, $dbPort, $dbUser, $dbPass);
    }

    /**
     * Check MySql Connexion Task
     *
     * @phpstan-return MySql\TouchServerTask
     *
     * @return CollectionBuilder
     */
    protected function taskMySqlTouchServerTask()
    {
        /** @phpstan-ignore-next-line  */
        return $this->task(MySql\TouchServerTask::class);
    }

    /**
     * Wait for MySql Connexion Task
     *
     * @phpstan-return MySql\WaitForServerTask
     *
     * @return CollectionBuilder
     */
    protected function taskMySqlWaitForServerTask()
    {
        /** @phpstan-ignore-next-line  */
        return $this->task(MySql\WaitForServerTask::class);
    }

    /**
     * Ensure MySql Database Task
     *
     * @phpstan-return MySql\EnsureDatabaseTask
     *
     * @return CollectionBuilder
     */
    protected function taskMySqlEnsureDatabaseTask()
    {
        /** @phpstan-ignore-next-line  */
        return $this->task(MySql\EnsureDatabaseTask::class);
    }

    /**
     * Create MySql Database Task
     *
     * @phpstan-return MySql\CreateDatabaseTask
     *
     * @return CollectionBuilder
     */
    protected function taskMySqlCreateDatabaseTask()
    {
        /** @phpstan-ignore-next-line  */
        return $this->task(MySql\CreateDatabaseTask::class);
    }

    /**
     * Drop MySql Database Task
     *
     * @phpstan-return MySql\DropDatabaseTask
     *
     * @return CollectionBuilder
     */
    protected function taskMySqlDropDatabaseTask()
    {
        /** @phpstan-ignore-next-line  */
        return $this->task(MySql\DropDatabaseTask::class);
    }

    /**
     * Execute MySql Database Query Task
     *
     * @phpstan-return MySql\ExecSqlTask
     *
     * @return CollectionBuilder
     */
    protected function taskMySqlExecTask()
    {
        /** @phpstan-ignore-next-line  */
        return $this->task(MySql\ExecSqlTask::class);
    }
}
